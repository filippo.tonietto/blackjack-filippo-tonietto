using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckBox : MonoBehaviour
{
    [Header("General")]
    public GameObject handedCard;
    public GameObject cardPrefab;
    public Shuffler shuffler;

    [Header("Next Deck")]
    public List<Card_SO> newDeck = new List<Card_SO>();

    [Header("Graphic")]
    public List<SpriteRenderer> visualCards = new List<SpriteRenderer>();
    int _drawNumber;

    Card _card;

    void Start()
    {
        // It creates a copy of the normal Deck, so that it can swap it when the first one ends.
        for (int i = 0; i < shuffler.deck.Count; i++)
        {
            newDeck.Add(shuffler.deck[i]);
        }
    }

    // That's the function that manages the drawing from the Deck.
    public void DrawCard()
    {
        // If the Dealer gives a Card from an ordered Deck, he makes a mistake.
        if(shuffler.ordered)
        {
            Dealer.Instance.RemoveLife();
        }

        var newCard = Instantiate(cardPrefab, handedCard.transform.position, handedCard.transform.rotation, handedCard.transform);
        _card = newCard.GetComponent<Card>();
        _card.SetInfo(shuffler.deck[0]);
        shuffler.RemoveDrawedCard();

        if(shuffler.deck.Count == 0)
        {
            for(int i = 0; i < newDeck.Count; i++)
            {
                shuffler.deck.Add(newDeck[i]);
            }

            shuffler.ShuffleFeedback();
        }

        var dealer = handedCard.GetComponentInParent<Dealer>();
        dealer.cardInHand = _card;
        StartCoroutine(WaitForCarded(dealer));

        #region Visual Cards Decrease
        _drawNumber++;
        if(_drawNumber % 5 == 0)
        {
            if (_drawNumber == 50) return;

            for(int i = visualCards.Count - 1; i >= 0; i--)
            {
                if (visualCards[i].enabled)
                {
                    visualCards[i].enabled = false;
                    break;
                }
                else continue;
            }
        }
        else if(_drawNumber == 52)
        {
            foreach(var c in visualCards)
            {
                c.enabled = true;
            }

            _drawNumber = 0;
        }
        #endregion

        //Debug.Log("Card Drawed!");
    }

    // This Coroutine is useful to avoid problems of execution order. 
    // It would in fact instantly throw the Card when drawing.
    IEnumerator WaitForCarded(Dealer dealer)
    {
        yield return new WaitForEndOfFrame();
        dealer.carded = true;
    }
}

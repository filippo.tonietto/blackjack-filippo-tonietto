using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

// A simple AudioManager, which is in part also a SettingsManager.
// I coded a more complex version of this for the game "Battle of Tezuma", made for the Project "The Big One" during my academic year at DBGA.
public class AudioManager : MonoBehaviour
{
    #region Singleton

    public static AudioManager Instance { get; private set; }


    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    #endregion

    public AudioMixer audioMixer;
    public AudioSource sfxAudioSource;
    public AudioSource musicAudioSource;
    public List<AudioClips> audioClips;

    public float mouseSensitivity;

    [System.Serializable]
    public struct AudioClips
    {
        public string name;
        public bool canPlay;
        public AudioClip audioClip;
    }

    private void Start()
    {
        if (musicAudioSource != null)
        {
            SearchAndPlayMusic("Menu Theme");
        }
    }

    public void SearchAndPlayMusic(string nameClip)
    {
        // Find index of audio clip and play background sounds.
        int audioIndex = audioClips.FindIndex(a => a.name == nameClip);
        if (audioClips[audioIndex].canPlay)
        {
            musicAudioSource.clip = audioClips[audioIndex].audioClip;
            musicAudioSource.Play();
        }
    }

    public void SearchAndPlaySfx(string nameClip)
    {
        // Find index of audio clip and play one shot.
        int audioIndex = audioClips.FindIndex(a => a.name == nameClip);
        if (audioClips[audioIndex].canPlay)
        {
            sfxAudioSource.PlayOneShot(audioClips[audioIndex].audioClip);
        }
    }

    public void SetMasterVolume(System.Single volume)
    {
        sfxAudioSource.volume = volume;
        musicAudioSource.volume = volume;
    }

    public void SetSensitivity(System.Single sensitivity)
    {
        mouseSensitivity = sensitivity;

        var fpsCam = Camera.main.GetComponent<FPSCamera>();
        if (fpsCam) fpsCam.mouseSensitivity = mouseSensitivity;
    }
}
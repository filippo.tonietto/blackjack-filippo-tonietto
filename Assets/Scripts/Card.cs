using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    public Card_SO cardSO;
    public SpriteRenderer sr;

    [Header("Card Throw")]
    public bool throwed;

    void Start()
    {

    }

    void Update()
    {

    }

    // Used at the Instatiation, to make the Card visually represents the Scriptable Object.
    public void SetInfo(Card_SO so)
    {
        cardSO = so;
        sr.sprite = cardSO.sprite;
    }

    // If the Card hits the invisible Colliders around the Table, it means it's going in the void.
    // An Heart is removed from the Dealer, while the Card is destroyed to avoid memory leak.
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 9)
        {
            Dealer.Instance.RemoveLife();
            Destroy(gameObject);
        }
    }
}

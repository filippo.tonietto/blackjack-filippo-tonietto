using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Shuffler : MonoBehaviour
{
    [Header("General")]
    public List<Card_SO> deck = new List<Card_SO>();
    public bool ordered;

    [Header("UI")]
    public GameObject balloon;
    public TextMeshPro deckText;

    void Start()
    {
        ordered = true;

        balloon.SetActive(true);
    }

    // FisherYates Algorithm.
    public void Shuffle()
    {
        for(int i = deck.Count - 1; i > 0; i--)
        {
            Card_SO temp = deck[i];
            int k = Random.Range(0, i + 1);
            deck[i] = deck[k];
            deck[k] = temp;
        }

        ordered = false;
        balloon.SetActive(false);

        AudioManager.Instance.SearchAndPlaySfx("Shuffle");
    }

    public void RemoveDrawedCard()
    {
        for(int i = 0; i < deck.Count - 1; i++)
        {
            deck[i] = deck[i + 1];
        }

        deck.RemoveAt(deck.Count - 1);
    }

    public void ShuffleFeedback()
    {
        balloon.SetActive(true);
        ordered = true;
        deckText.text = "Deck changed, shuffle first!";
    }
}

﻿// A simple class (could have been a struct) to represent a Turn.
public class Turn
{
    public TurnType type;
    public int number;
}

public enum TurnType
{
    dealer = 0,
    players = 1,
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Card", order = 1)]
public class Card_SO : ScriptableObject
{
    [SerializeField] public int value;
    [SerializeField] public string seed;
    [SerializeField] public Sprite sprite;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsSlider : MonoBehaviour
{
    Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
        if(slider.maxValue == 1)
        {
            slider.onValueChanged.AddListener(AudioManager.Instance.SetMasterVolume);
            slider.value = AudioManager.Instance.musicAudioSource.volume;
        }
        else
        {
            slider.onValueChanged.AddListener(AudioManager.Instance.SetSensitivity);
            slider.value = AudioManager.Instance.mouseSensitivity;
        }
    }
}

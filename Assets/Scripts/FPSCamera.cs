using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A simple FPS Camera Controller.
// I coded this for the game "Chuck's Conquest", made for the Project "Twister" during my academic year at DBGA.
public class FPSCamera : MonoBehaviour
{
    public float mouseSensitivity;

    float xRotation = 0f;
    float yRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        mouseSensitivity = AudioManager.Instance.mouseSensitivity;
    }

    void Update()
    {
        if (GameManager.Instance.paused) return;

        float horiz = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float vert = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= vert;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        yRotation += horiz;

        transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
    }
}

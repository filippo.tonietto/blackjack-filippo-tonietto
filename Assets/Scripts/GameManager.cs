using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    #region Singleton

    public static GameManager Instance { get; private set; }


    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(this);
    }

    #endregion

    [Header("General")]
    public List<Player> players = new List<Player>();
    public Turn currentTurn;
    public float remainingTime;
    [HideInInspector] public int winners;
    
    [Header("UI Manager")]
    public TextMeshProUGUI endingUI;
    public TextMeshProUGUI restartCounter;
    public TextMeshProUGUI timerText;
    public GameObject gameOverPanel;
    public GameObject pausePanel;
    public bool paused;

    [Header("Player Manager")]
    public GameObject playerPrefab;
    public List<GameObject> positions = new List<GameObject>();
    public int totalPlayers;

    TimeSpan _timeSpan;

    void Start()
    {
        currentTurn = new Turn();
        currentTurn.type = TurnType.players;
        currentTurn.number = 1;

        remainingTime = 60f;

        StartCoroutine(Spawner(2));

        Debug.LogWarning($"Current Turn: {currentTurn.type}, Turn Number: {currentTurn.number}");
    }

    void Update()
    {
        // UI Management: check if it has to open or close the Pause Menu.
        if(Input.GetKeyDown(KeyCode.Escape) && !gameOverPanel.activeInHierarchy)
        {
            if(paused)
            {
                Resume();
            }
            else
            {
                PausePanel();
            }
        }
        
        // The countdown for the remaining time. If it goes under 0, it's Game Over.
        if(remainingTime > 0)
        {
            if (paused) return;
            remainingTime -= Time.deltaTime;
            _timeSpan = TimeSpan.FromSeconds(remainingTime);
            timerText.text = $"Remaining time: {_timeSpan.ToString(@"m\:ss")}";
        }
        else
        {
            if (gameOverPanel.activeInHierarchy) return;
            GameOverPanel();
        }
    }

    // A function made to Spawn a variable number of Players at a time.
    IEnumerator Spawner(int number)
    {
        while(number > 0)
        {
            SpawnPlayer();
            number--;
            yield return null;
        }
    }

    // The actual spawning function.
    public void SpawnPlayer()
    {
        totalPlayers++;

        var temp = Instantiate(playerPrefab);
        var newPlayer = temp.GetComponent<Player>();

        newPlayer.gameObject.name = $"Player{totalPlayers}"; 
        newPlayer.SkinAndName();
        newPlayer.ChoosePosition();
        
        players.Add(newPlayer);
    }

    // Function that manages the possibility for Players to go away from the Table.
    public void RemovePlayer()
    {
        if (players.Count <= 2) return;

        foreach(var p in positions)
        {
            var player = p.GetComponentInChildren<Player>();
            if (!player) continue;

            float random = UnityEngine.Random.Range(0f, 1.0f);
            if(random <= 0.10f)
            {
                players.Remove(player);
                Destroy(player.gameObject);
            }
        }
    }

    // The function that manages the Turns rotation. 
    // It's "Player's Turn" until every Player is ok with its number of Cards (or gets a bust).
    // It gets called by every Player after their Think() function.
    public void ChangeTurn()
    {
        int think = 0;
        for(int i = 0; i < players.Count; i++)
        {
            if (players[i].think) think++;
        }

        // Not everyone thinked yet. So no Turn change.
        if (think < players.Count) return;

        int satisfied = 0;
        for(int i = 0; i < players.Count; i++)
        {
            if (players[i].satisfied)
            {
                satisfied++;
            } else 
            { 
                players[i].think = false; 
            }
        }

        // If every Player is ok with its number of Cards, now it's Dealer's Turn.
        if(satisfied == players.Count)
        {
            currentTurn.number++;
            currentTurn.type = TurnType.dealer;

            Dealer.Instance.balloon.SetActive(true);
        }
        else // ... It's still Players' Turn.
        {
            currentTurn.number++;
            currentTurn.type = TurnType.players;
        }

        Debug.LogWarning($"Current Turn: {currentTurn.type}, Turn Number: {currentTurn.number}");
    }

    // Function that manages what happens when also the Dealer placed all his cards, so the BlackJack Game ends.
    public void EndGame()
    {
        string winnersNames = "";
        int highest = 0;
        int bustedPlayers = 0;
        
        endingUI.gameObject.SetActive(true);

        AudioManager.Instance.SearchAndPlaySfx("Match Ended");

        // Check how many Players lose for sure (got bust), and what's the highest score of the remainings.
        foreach (var p in players)
        {
            if (p.busted)
            {
                bustedPlayers++;
                continue;
            }

            if (highest < p.points)
            {
                highest = p.points;
            }
        }

        // Every Player got bust. So we have to check if it's Dealer's Win or if he got bust too.
        if(bustedPlayers == players.Count)
        {
            if(Dealer.Instance.points <= 21)
            {
                endingUI.text = "Dealer wins!";
                Dealer.Instance.AddLife();
                remainingTime += 10f;
            }
            else
            {
                endingUI.text = "No win!";
                remainingTime -= 10f;
            }

            StartCoroutine(ResetGame());
            return;
        }

        // Next Option: not everyone got bust, but the Dealer did.
        if (Dealer.Instance.points > 21)
        {
            int counter = 0;
            foreach(var p in players)
            {
                if(p.points <= 21)
                {
                    p.winner = true;
                    p.anim.SetBool("Win", true);

                    if (counter == 0)
                    {
                        winnersNames += $"{p.gameObject.name}";
                        counter++;
                    }
                    else winnersNames += $" & {p.gameObject.name}";
                }
            }

            if(winnersNames.Contains("&"))
            {
                endingUI.text = $"{winnersNames} win!";
            } 
            else endingUI.text = $"{winnersNames} wins!";

            remainingTime -= 5f;
        }
        // Next Option: neither the Dealer got bust.
        else
        {
            // If the Dealer has the highest points, that means he won.
            if(Dealer.Instance.points > highest)
            {
                endingUI.text = "Dealer wins!";
                Dealer.Instance.AddLife();
                remainingTime += 10f;
            }
            else
            {
                int counter = 0;
                foreach (var p in players)
                {
                    if (p.points == highest)
                    {
                        p.winner = true;
                        p.anim.SetBool("Win", true);

                        if (counter == 0)
                        {
                            winnersNames += $"{p.gameObject.name}";
                            counter++;
                        }
                        else winnersNames += $" & {p.gameObject.name}";
                    }
                }

                if (winnersNames.Contains("&"))
                {
                    endingUI.text = $"{winnersNames} win!";
                }
                else endingUI.text = $"{winnersNames} wins!";

                remainingTime += 5f;
            }
        }

        foreach (var p in players)
        {
            if (!p.winner) p.anim.SetBool("Defeat", true);
        }

        // I decided that there's a low chance for the Dealer to get an extra life, even if he did not win.
        float random = UnityEngine.Random.Range(0f, 1f);
        if(random < 0.25f) Dealer.Instance.AddLife();

        StartCoroutine(ResetGame());
    }

    // Coroutine that resets the Turns count, the UI, and that calls the reset of the Dealer, of the Players
    IEnumerator ResetGame()
    {
        float waitTime = 3f;

        while(waitTime >= 0.4)
        {
            waitTime -= Time.deltaTime;
            restartCounter.text = $"New game in... {Mathf.CeilToInt(waitTime)}";
            yield return null;
        }

        endingUI.gameObject.SetActive(false);

        RemovePlayer();

        foreach(var p in players)
        {
            p.ResetPlayer();
        }
        Dealer.Instance.ResetDealer();

        currentTurn.number = 1;
        currentTurn.type = TurnType.players;

        // We the finally Spawn a random number of new Players.
        int random = UnityEngine.Random.Range(0, 3);
        StartCoroutine(Spawner(random));
    }

    #region User Interface Management
    public void GameOverPanel()
    {
        paused = true;
        Time.timeScale = 0f;

        AudioManager.Instance.musicAudioSource.Pause();
        AudioManager.Instance.SearchAndPlaySfx("Game Over");

        gameOverPanel.SetActive(true);
        pausePanel.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

        Cursor.lockState = CursorLockMode.Locked;
        AudioManager.Instance.SearchAndPlayMusic("Game Theme");
        Time.timeScale = 1f;
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);

        AudioManager.Instance.SearchAndPlayMusic("Menu Theme");
        Time.timeScale = 1f;
    }

    public void PausePanel()
    {
        AudioManager.Instance.musicAudioSource.Pause();
        AudioManager.Instance.sfxAudioSource.Pause();

        pausePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        paused = true;
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        AudioManager.Instance.musicAudioSource.UnPause();
        AudioManager.Instance.sfxAudioSource.UnPause();

        pausePanel.SetActive(false);
        Time.timeScale = 1f;
        paused = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    #endregion
}

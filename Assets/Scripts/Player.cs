using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    [Header("General")]
    public int cards;
    public int points;
    public List<GameObject> cardPos = new List<GameObject>();

    [Header("AI")]
    [Range(1, 10)]
    public int intelligence;
    public bool think;
    public bool satisfied;
    public bool busted;
    public bool winner;

    [Header("User Interface")]
    public TextMeshPro nameUI;
    public TextMeshPro pointsUI;
    public GameObject balloon;
    public GameObject card_bal;
    public GameObject bust_bal;

    [Header("Skins")]
    public List<GameObject> skins = new List<GameObject>();

    [Header("Bools")]
    bool ballooned;

    [HideInInspector] public Animator anim;
    [HideInInspector] public AudioSource source;

    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        source = GetComponent<AudioSource>();
        
        // Setting the value of intelligence at the start of every new Player.
        intelligence = Random.Range(1, 11);
    }

    void Start()
    {
        points = 0;
        pointsUI.text = $"{points}";

        StartCoroutine(FirstBalloon());
    }

    void Update()
    {
        if (GameManager.Instance.paused) return;

        if (ballooned)
        {
            StartCoroutine(Think());
            ballooned = false;
        }
    }

    #region Initial Setup
    public void SkinAndName()
    {
        int random = Random.Range(0, skins.Count);

        for(int i = 0; i < skins.Count; i++)
        {
            if(i == random) skins[i].SetActive(true);
            else skins[i].SetActive(false);
        }

        nameUI.text = gameObject.name;
    }

    public void ChoosePosition()
    {
        for(int i = 0; i < GameManager.Instance.positions.Count; i++)
        {
            if (GameManager.Instance.positions[i].GetComponentInChildren<Player>()) continue;

            transform.position = GameManager.Instance.positions[i].transform.localPosition;
            transform.parent = GameManager.Instance.positions[i].transform;
            anim.transform.LookAt(Dealer.Instance.transform);
        }
    }
    #endregion

    // Every time the Dealer moves, and at the Start, the Players look at him.
    public void LookAtDealer()
    {
        anim.transform.LookAt(Dealer.Instance.transform);
    }

    // A Card collided with the Player.
    private void OnTriggerEnter(Collider other)
    {
        if (!card_bal.activeInHierarchy) return;

        cards++;

        for(int i = 0; i < cardPos.Count; i++)
        {
            if (cardPos[i].GetComponentInChildren<Card>()) continue;
            var obj = other.gameObject;
            obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            obj.transform.parent = cardPos[i].transform;
            obj.transform.position = cardPos[i].transform.position;
            obj.transform.rotation = cardPos[i].transform.rotation;
            break;
        }

        var card = other.gameObject.GetComponent<Card>();

        // Drawed an Ace
        if(card.cardSO.value == -1)
        {
            // On the first turn, by default Ace value will be 11
            if(GameManager.Instance.currentTurn.number == 1)
            {
                card.cardSO.value = 11;
            }
            // On other turns, Player can decide if making it value 11 or 1
            else
            {
                if (points >= 17) card.cardSO.value = 1;
                else card.cardSO.value = 11;
            }
        }

        points += card.cardSO.value;

        if(points <= 21)
        {
            StartCoroutine(GetCardAnim());
        } 
        else
        {
            anim.SetBool("Bust", true);
        }
        
        GameManager.Instance.remainingTime += 2f;

        AudioManager.Instance.SearchAndPlaySfx("Card");

        pointsUI.text = $"{points}";
    }

    IEnumerator GetCardAnim()
    {
        anim.SetBool("Card", true);
        yield return new WaitForSecondsRealtime(0.5f);
        anim.SetBool("Card", false);
    }

    // First time a Player wants a Card.
    IEnumerator FirstBalloon()
    {
        yield return new WaitForSecondsRealtime(1f);
        OpenCloseBalloon(true, 0);
        yield return new WaitUntil(() => cards > 0);
        OpenCloseBalloon(false, -1);
        ballooned = true;
    }

    // The Player wants a Card.
    IEnumerator GenericBalloon()
    {
        OpenCloseBalloon(true, 0);
        var curCards = cards;
        yield return new WaitUntil(() => cards != curCards);
        OpenCloseBalloon(false, -1);
        ballooned = true;
    }

    // The actual AI of the Player.
    IEnumerator Think()
    {
        // Check if exceeded 21.
        if (points >= 21)
        {
            satisfied = true;
            think = true;
            GameManager.Instance.ChangeTurn();
            if (points > 21)
            {
                OpenCloseBalloon(true, 1);
                busted = true;
                source.PlayOneShot(source.clip);
            }
            yield break;
        }

        yield return new WaitForSecondsRealtime(2f);
        think = true;

        // Divide the thinking method between first turn and other turns.
        if(cards == 0)
        {
            StartCoroutine(GenericBalloon());
        }
        else // Divide the thinking method relative to the current points.
        {
            if (points < 14)
            {
                // Wants to Draw.
                StartCoroutine(GenericBalloon());
            }
            else if (points >= 14 && points < 16)
            {
                if (intelligence > 4) // Done with Cards.
                {
                    satisfied = true;
                }
                else
                {
                    // Wants to Draw.
                    StartCoroutine(GenericBalloon());
                }
            }
            else if(points >= 16 && points < 19)
            {
                if (intelligence > 3) // Done with Cards.
                {
                    satisfied = true;
                }
                else
                {
                    // Wants to Draw.
                    StartCoroutine(GenericBalloon());
                }
            }
            else if(points >= 19 && points <= 21)
            {
                if (intelligence > 1) // Done with Cards.
                {
                    satisfied = true;
                }
                else
                {
                    // Wants to Draw.
                    StartCoroutine(GenericBalloon());
                }
            }
        }

        GameManager.Instance.ChangeTurn();
    }

    // Function that resets the Player at the end of every BlackJack's Game.
    public void ResetPlayer()
    {
        cards = 0;
        points = 0;

        OpenCloseBalloon(false, -1);

        List<Card> oldCards = new List<Card>();

        for (int i = 0; i < cardPos.Count; i++)
        {
            if (!cardPos[i].GetComponentInChildren<Card>()) continue;
            else
            {
                oldCards.Add(cardPos[i].GetComponentInChildren<Card>());
            }
        }

        for (int i = 0; i < oldCards.Count; i++)
        {
            Destroy(oldCards[i].gameObject);
        }

        think = false;
        satisfied = false;
        winner = false;
        busted = false;

        anim.SetBool("Win", false);
        anim.SetBool("Defeat", false);
        anim.SetBool("Bust", false);

        pointsUI.text = 0.ToString();

        StartCoroutine(FirstBalloon());
    }
    
    public void OpenCloseBalloon(bool power, int type)
    {
        // Turn it On
        if(power)
        {
            balloon.SetActive(true);
        }
        // Turn it Off
        else
        {
            balloon.SetActive(false);
        }

        // 'Wants a Card' Feedback
        if(type == 0)
        {
            card_bal.SetActive(true);
            bust_bal.SetActive(false);
        }
        // 'Busted' Feedback
        else if(type == 1)
        {
            bust_bal.SetActive(true);
            card_bal.SetActive(false);
        }
        else if(type == -1)
        {
            card_bal.SetActive(false);
            bust_bal.SetActive(false);
        }
    }
}

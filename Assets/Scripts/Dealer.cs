using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dealer : MonoBehaviour
{
    #region Singleton

    public static Dealer Instance { get; private set; }


    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(this);
    }

    #endregion

    [Header("General")]
    public Card cardInHand;
    public bool carded;

    [Header("Dealer's Game")]
    public int cards;
    public int points;
    public bool winner;
    public List<GameObject> dealerCards = new List<GameObject>();

    [Header("UI")]
    public TextMeshPro pointsUI;
    public GameObject balloon;
    public GameObject mark_bal;
    public GameObject card_bal;

    [Header("Lifes")]
    public int life = 3;
    public List<Image> hearts = new List<Image>();

    SphereCaster _caster;

    void Start()
    {
        _caster = GetComponent<SphereCaster>();

        pointsUI.text = 0.ToString();
    }

    // Here is managed the throwing action.
    void Update()
    {
        if (GameManager.Instance.paused) return;

        if (Input.GetMouseButtonDown(0))
        {
            // If Dealer has not a Card in his hand, it cannnot trow anything.
            if (!carded) return;

            // Check if we are pointing something that interacts differently at mouse click.
            if(_caster.currentHitObject)
            {
                if (_caster.currentHitObject.CompareTag("DealerCards") ||
                _caster.currentHitObject.CompareTag("Platform")) return;
            }

            // The proper action of throwing, applying some forces at Card's Rigidbody.
            var rb = cardInHand.GetComponent<Rigidbody>();
            rb.AddTorque(cardInHand.transform.right * 20f, ForceMode.VelocityChange);
            rb.AddForce(Camera.main.transform.forward * 20f, ForceMode.VelocityChange);

            cardInHand.transform.parent = null;
            cardInHand.throwed = true;
            cardInHand = null;
            carded = false;
        }
    }

    // In this function rules the action of posing a Dealer's Card inside the proper area.
    public void DealerCards()
    {
        cards++;
        for (int i = 0; i < dealerCards.Count; i++)
        {
            if (dealerCards[i].GetComponentInChildren<Card>()) continue;
            cardInHand.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            cardInHand.transform.parent = dealerCards[i].transform;
            cardInHand.transform.position = dealerCards[i].transform.position;
            cardInHand.transform.rotation = dealerCards[i].transform.rotation;
            break;
        }

        // Drawed an Ace (which can be both 1 or 11, at Player's/Dealer's discretion.
        if (cardInHand.cardSO.value == -1)
        {
            if(points > 10)
            {
                cardInHand.cardSO.value = 1;
            }
            else
            {
                cardInHand.cardSO.value = 11;
            }
        }

        points += cardInHand.cardSO.value;

        GameManager.Instance.remainingTime++;

        // Check if the Dealer reached his goal.
        if (points >= 17)
        {
            GameManager.Instance.EndGame();

            balloon.SetActive(false);
        }

        pointsUI.text = $"{points}";
        
        AudioManager.Instance.SearchAndPlaySfx("Card");

        cardInHand = null;
        carded = false;
    }

    // This function resets all the values of the Dealer; used when a Game is over.
    public void ResetDealer()
    {
        List<Card> oldCards = new List<Card>();

        for (int i = 0; i < dealerCards.Count; i++)
        {
            if (!dealerCards[i].GetComponentInChildren<Card>()) continue;
            else
            {
                oldCards.Add(dealerCards[i].GetComponentInChildren<Card>());
            }
        }

        for(int i = 0; i < oldCards.Count; i++)
        {
            Destroy(oldCards[i].gameObject);
        }

        cards = 0;
        points = 0;
        
        pointsUI.text = 0.ToString();
        
        winner = false;
    }

    // Function called every time Dealer loses an Heart.
    // It also check if it is a Game Over.
    public void RemoveLife()
    {
        life--;
        hearts[life].color = Color.black;

        if(life == 0)
        {
            GameManager.Instance.GameOverPanel();
        }
    }

    // Function called every time Dealer gains an Heart.
    public void AddLife()
    {
        if (life >= 3) return;
        life++;
        hearts[life - 1].color = Color.white;
    }

    // Function called to Teleport Dealer to the next point.
    public void Teleport(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;

        foreach(var p in GameManager.Instance.players)
        {
            p.LookAtDealer();
        }
    }
}

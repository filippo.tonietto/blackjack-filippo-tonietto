using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereCaster : MonoBehaviour
{
    public GameObject currentHitObject;
    float currentHitDistance;

    Vector3 origin;
    Vector3 direction;
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;

    GameObject mainCamera;

    public Outline pointer;

    void Start()
    {
        mainCamera = Camera.main.gameObject;
    }

    void Update()
    {
        if (GameManager.Instance.paused) return;

        origin = mainCamera.transform.position;
        direction = mainCamera.transform.forward;

        RaycastHit hit;

        if(Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal))
        {
            currentHitObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;

            if(currentHitObject.layer == 6) // If the object we are seeing is Interactable;
            {
                if(currentHitObject.tag == "Platform")
                {
                    pointer.enabled = true;

                    if (Input.GetMouseButtonDown(0))
                    {
                        Dealer.Instance.Teleport(currentHitObject.transform.parent.position, currentHitObject.transform.parent.rotation);
                    }
                }

                if(!Dealer.Instance.carded) // We want to make the Shuffler and the DeckBox interactable only if the Dealer has not Cards in hand.
                {
                    if (currentHitObject.tag == "ShuffleButton")
                    {
                        pointer.enabled = true;

                        if (Input.GetMouseButtonDown(0))
                        {
                            currentHitObject.GetComponent<Shuffler>().Shuffle();
                        }
                    }
                    else if (currentHitObject.tag == "DeckBox")
                    {
                        pointer.enabled = true;

                        if (Input.GetMouseButtonDown(0))
                        {
                            currentHitObject.GetComponent<DeckBox>().DrawCard();
                        }
                    }
                }
                else if(GameManager.Instance.currentTurn.type == TurnType.dealer) // If we are pointing to the Dealer's Cards Platform.
                {
                    if (currentHitObject.tag == "DealerCards")
                    {
                        pointer.enabled = true;

                        if (Input.GetMouseButtonDown(0))
                        {
                            Dealer.Instance.DealerCards();
                        }
                    }
                }
            }
            else if(currentHitObject.layer == 7) // If we are pointing to a near Player the pointer flashes.
            {
                if (!Dealer.Instance.carded) return;
                pointer.enabled = true;
            }
            else
            {
                pointer.enabled = false;
            }
        }
        else // If we are pointing the void.
        {
            currentHitObject = null;
            currentHitDistance = maxDistance;
            pointer.enabled = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }
}
